# Charge Full Stack API Take Home

### Usage
Simply run docker-compose

`docker-compose up`

Also, service is deployed to 

`http://charge.petrosyan.in/organizations`

### POST data example
```json
{
  "identifier": "niels.bohr@quantum.com",
  "business": {
    "dba": "Quantum",
    "legalBusinessName": "Quantum Corp",
    "ein": "124-26-26",
    "billingPhoneNumber": 3238676895,
    "businessEmail": "niels.bohr@quantum.com",
    "nameOfMainContact": "Edwin Hubble",
    "billingStreetName": "123 Atom Street 5",
    "billingCity": "Los Angeles",
    "billingState": "CA",
    "billingZip": "90026",
    "billingCountry": "US",
    "timezone": "PST",
    "advertisingMethod": "brochure",
    "whichHostProvider": "Heroku",
    "isBusinessSeasonal": true,
    "monthsClosed": [
      "jan",
      "feb"
    ],
    "depositRequired": true,
    "valueOfDepositRequired": 5000,
    "refundPolicy": "full_refund",
    "warrantee": "no",
    "useFulfillment": true,
    "vendor": {
      "vendorName": "Quantum Inc.",
      "contactName": "Albert Einstein",
      "phoneNumber": 3238676895,
      "address": "145 Electro Road",
      "city": "Los Angeles",
      "state": "CA",
      "zip": "90266",
      "country": "US"
    },
    "describeTransactionToFulfillment": "Order is placed online then send to fulfillment."
  },
  "structure": {
    "howManyEmployees": 3,
    "typeOfBusiness": "vape",
    "typeOfBusinessOwnership": "government",
    "stateOfFormation": "CA",
    "businessStartDate": "2019-12-20",
    "typeOfLocation": "business_district | retail",
    "squareFootageOffice": "0-250",
    "numberOfLocations": 2,
    "priorBankruptcy": false,
    "detailedDescriptionOfProducts": "The business sells quantum cats"
  },
  "commerce": {
    "creditCardsSwiped": 17585,
    "creditCardPresent": 5089,
    "mailPhoneOrder": 7898,
    "ecommerce": 4598,
    "ecommerceUrl": "https://quantum.com",
    "whenWillSiteBeLive": "2019-12-20",
    "merchantType": "ecommerce",
    "ecommerceType": [
      "auction"
    ],
    "howManyDaysProductArrives": "over_30",
    "deliveryMethod": "fedex",
    "useShoppingCart": "shopify",
    "acceptCreditCards": true,
    "whenIsCardCharged": "date_of_delivery",
    "currentProcessor": "Shopify",
    "reasonForLeaving": "rate",
    "visaMonthlyVolume": 2453,
    "mastercardMonthlyVolume": 12450,
    "annualVisaMCVolume": 25000,
    "amexMonthlyVolume": 0,
    "amexAnnualVolume": 13452,
    "discoverMonthlyVolume": 12450,
    "discoverAnnualVolume": 14520,
    "totalMonthlyVolume": 152352,
    "totalAnnualVolume": 125000,
    "averageTicket": 1250,
    "highestTicket": 12500,
    "chargeCustomersOnRegularBasis": true,
    "thirdPartyDataStore": true,
    "totalB2BSales": "45.5",
    "totalB2CSales": "67.56",
    "bankName": "Orbital Bank",
    "bankContactName": "Marie Curie",
    "bankPhoneNumber": 3235684912,
    "bankAccountNumber": 122200234,
    "bankRoutingNumber": 4258625821
  },
  "persons": [
    {
      "identifier": "niels.bohr@quantum.com",
      "jobTitle": "ceo",
      "percentOwnership": 99.99,
      "billingFirstName": "Niels",
      "billingLastName": "Bohr",
      "email": "niels.bohr@quantum.com",
      "phoneNumber": "3283284141",
      "billingStreetName": "123 Atom Street 5",
      "billingCity": "Copenhagen",
      "billingState": "CA",
      "billingZip": "90026",
      "billingCountry": "US",
      "yearsAtResidence": 10,
      "monthsAtResidence": 5,
      "rentOrOwn": "rent",
      "dob": "1885-10-7",
      "documentType": "passport",
      "idNumber": "14gdg3435",
      "docState": "CA",
      "documentFront": "U3dhZ2dlciByb2Nrcw==",
      "documentBack": "U3dhZ2dlciByb2Nrcw==",
      "faceData": "U3dhZ2dlciByb2Nrcw==",
      "ssn": "604-72-6932"
    }
  ]
}
```


Design and code a two endpoint API Server using the following specs:

`POST /organizations`

https://openapi.charge-dev.net/#operation/post-kyb-organization

`GET /organizations`

https://openapi.charge-dev.net/#operation/get-kyb-organizations

To Submit your solution:

- clone this repo

- push your solution to a public repo

## Tech Requirements

**Server**

[NodeJs](https://nodejs.org/en/) & [Express](https://expressjs.com/)

**Database**

[Postgres](https://www.postgresql.org/) & [Sequelize](https://sequelize.org/)

**Message Queue**

[bullmq](https://github.com/taskforcesh/bullmq) & [redis](https://redis.io/)

**Suggested**

[Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) for orchestration

## Logic Requirements:

**Authorization**
- Provide a hard coded API Key for authentication

**Query Parameters**
- Ignore the `matrix-rules` parameter
- if `process=true` place the request body in a message queue 
- if `process=false` simply return the response
- if `data=none`, the `data` object in the response should be empty
- if `data=base64`, the `data` object in the response should be the request body in base 64 encoded
- if `data=full`, the `data` object in the response should be the entire request body

**Request Body**
- for the `POST` request body, simply use the body provided in the spec
- validate data structure to see if fields are not provided

**Response Object**
For the response of the `POST` and `GET` requests:

- if `process=true`:

the `result` object in the response should be:
```json
"result": {
"status": "processing",
"score": 0,
"statusHistory": [
    {
        "status": "processing",
        "createdAt": "2020-08-09 04:05:02",
        "user": {
            "id": "775671ff-f70d-415d-b523-f50b05139ac8",
            "identifier": "max.planck@gravity.com"
        }
    },
    {
        "status": "created",
        "createdAt": "2020-08-09 04:05:02",
        "user": {
            "id": "775671ff-f70d-415d-b523-f50b05139ac8",
            "identifier": "max.planck@gravity.com"
        }
    }
],
"checks": []
}
```

- if `process=false`:

the `result` object in the response should be:
```json
"result": {
"status": "review",
"score": 0,
"statusHistory": [
    {
        "status": "review",
        "createdAt": "2020-08-09 04:05:02",
        "user": {
            "id": "775671ff-f70d-415d-b523-f50b05139ac8",
            "identifier": "max.planck@gravity.com"
        }
    },
    {
        "status": "created",
        "createdAt": "2020-08-09 04:05:02",
        "user": {
            "id": "775671ff-f70d-415d-b523-f50b05139ac8",
            "identifier": "max.planck@gravity.com"
        }
    }
],
"checks": []
}
```
- the `source` object should indicate what user sent the request
- the `checks` array in the response should be empty
- the `notes` array in the response should be empty
- use valid time stamps 

**Response Status**

`POST`

- Only code for `201`, `400`, and `401`.

`GET`

- Only code for `200` and `401`.

**Data Structures**

You can organize the data structures in any way you want. I recommend you start with the 4 main objects in the request body:

- `business`
- `structure`
- `commerce`
- `person`

## Bonus

Write a one command cli in [go](https://golang.org/) that allows for making the post request:

```
charge post organizations --authorization=MY_API_KEY --body=body.json
```

## Questions

How would you scale this API to process millions of requests?

What you think would make this API more secure or easily extensible?
