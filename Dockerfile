FROM node:12

WORKDIR /home/project

COPY package*.json ./

RUN npm ci
COPY . .

EXPOSE 3000
CMD [ "node", "src/index.js" ]
