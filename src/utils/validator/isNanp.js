const nanpRegex = /([2-9][0-9]{2}){2}[0-9]{4}/g;

module.exports = function (value) {
  const phoneNumber = value.toString().split('-').join('');
  if(!phoneNumber.match(nanpRegex)) {
    throw new Error('Should be in NANP format');
  }
};
