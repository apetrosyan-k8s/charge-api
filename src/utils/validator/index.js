const isNanp = require('./isNanp');
const isCountryCode = require('./isCountryCode');

module.exports = {
  isNanp,
  isCountryCode,
}
