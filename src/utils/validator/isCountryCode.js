const iso = require('iso-3166-1-alpha-2')

module.exports = function (value) {
  if(!iso.getCountry(value)) {
    throw new Error('Should be country code in ISO 3166-1 alpha-2 format');
  }
};
