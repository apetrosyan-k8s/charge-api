function validate(record) {
  const includedNames = (record._options || {}).includeNames || [];
  const validatorArray =
    [
      record.validate(),
      ...includedNames.map(includeName => {
        if(Array.isArray(record[includeName])) {
          return Promise.all(record[includeName].map(rec => validate(rec)))
        }
        return validate(record[includeName]);
      })
    ];

  return Promise.all(validatorArray)
}

module.exports = validate;
