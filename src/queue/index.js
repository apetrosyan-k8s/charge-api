const { Queue } = require('bullmq');
const config = require('../utils/config')();

const queue = new Queue('Jobs', {
  connection: {
    host: config.REDIS_HOST,
    port: config.REDIS_PORT,
    password: config.REDIS_PASSWORD,
  }
});

module.exports = queue;
