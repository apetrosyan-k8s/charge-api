const Identity = require('./Identity');
const Business = require('./Business');
const Vendor = require('./Vendor');
const Structure = require('./Structure');
const Commerce = require('./Commerce');
const Person = require('./Person');
const { sequelize } = require('../database');

Identity.associate(sequelize.models);
Business.associate(sequelize.models);
Vendor.associate(sequelize.models);
Commerce.associate(sequelize.models);
Person.associate(sequelize.models);

module.exports = {
  Identity,
  Business,
  Vendor,
  Structure,
  Commerce,
  Person,
};
