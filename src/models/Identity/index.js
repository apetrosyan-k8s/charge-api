const Sequelize = require('sequelize');
const { sequelize } = require('../../database');
const { DataTypes } = Sequelize;

const Identity = sequelize.define('identity', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
  },
  identifier: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [5],
    },
  },
  status: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  score: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
});

Identity.associate = (models) => {
  Identity.hasOne(models.business);
  Identity.hasOne(models.structure);
  Identity.hasOne(models.commerce);
  Identity.hasMany(models.person, { as: 'persons' });
};

module.exports = Identity;
