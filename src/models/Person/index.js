const Sequelize = require('sequelize');
const { sequelize } = require('../../database');
const { isNanp, isCountryCode } = require('../../utils/validator');
const { DataTypes } = Sequelize;

const Person = sequelize.define('person', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
  },
  identifier: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [5]
    },
  },
  jobTitle: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['ceo', 'director', 'owner', 'partner', 'president', 'secretary', 'treasurer', 'vice_president']],
    },
  },
  percentOwnership: {
    type: DataTypes.FLOAT,
  },
  billingFirstName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2]
    },
  },
  billingLastName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2]
    },
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isEmail: true,
    },
  },
  phoneNumber: {
    type: DataTypes.STRING,
    validate: {
      isNanp,
    },
  },
  billingStreetName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  billingCity: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2]
    },
  },
  billingState: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2]
    },
  },
  billingZip: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2]
    },
  },
  billingCountry: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isCountryCode,
    },
  },
  yearsAtResidence: {
    type: DataTypes.INTEGER,
  },
  monthsAtResidence: {
    type: DataTypes.INTEGER,
  },
  rentOrOwn: {
    type: DataTypes.STRING,
    validate: {
      isIn: [['rent', 'own']],
    },
  },
  dob: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isDate: true,
    },
  },
  documentType: {
    type: DataTypes.STRING,
    validate: {
      isIn: [['driver_license', 'passport']],
    },
  },
  idNumber: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  docState: {
    type: DataTypes.STRING,
    validate: {
      isCountryCode,
      len: [2],
    },
  },
  documentFront: {
    type: DataTypes.STRING,
  },
  documentBack: {
    type: DataTypes.STRING,
  },
  faceData: {
    type: DataTypes.STRING,
  },
  ssn: {
    type: DataTypes.STRING,
  },
});

Person.associate = (models) => {
  Person.belongsTo(models.identity);
};

module.exports = Person;
