const Sequelize = require('sequelize');
const { sequelize } = require('../../database');
const { isNanp } = require('../../utils/validator');
const { DataTypes } = Sequelize;

const Commerce = sequelize.define('commerce', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
  },
  creditCardsSwiped: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  creditCardPresent: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  mailPhoneOrder: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  ecommerce: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  ecommerceUrl: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  whenWillSiteBeLive: {
    type: DataTypes.STRING,
    validate: {
      isDate: true,
      shouldDateBePresent(value) {
        if(!value && !this.ecommerceUrl) {
          throw new Error('whenWillSiteBeLive should be filled when ecommerceUrl is empty');
        }
      }
    },
  },
  merchantType: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['ecommerce', 'moto', 'retail']],
    },
  },
  ecommerceType: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    validate: {
      shouldBePresent(value) {
        if(!value && this.merchantType === 'ecommerce') {
          throw new Error('ecommerceType should be filled when merchantType is ecommerce');
        }
      }
    },
  },
  howManyDaysProductArrives: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['same_day', '1-5', '6-15', '16-30', 'over_30']],
    },
  },
  deliveryMethod: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['fedex', 'internet', 'ups', 'usps']],
    },
  },
  useShoppingCart: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['no', 'konnektive', 'limelight', 'shopify', 'woocommerce', 'zencart', 'other']],
    },
  },
  acceptCreditCards: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  whenIsCardCharged: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['date_of_delivery', 'date_of_order']],
    },
  },
  currentProcessor: {
    type: DataTypes.STRING,
  },
  reasonForLeaving: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['rate', 'service', 'terminated']],
    },
  },
  visaMonthlyVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  mastercardMonthlyVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  annualVisaMCVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  amexMonthlyVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  amexAnnualVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  discoverMonthlyVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  discoverAnnualVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  totalMonthlyVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  totalAnnualVolume: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  averageTicket: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  highestTicket: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  chargeCustomersOnRegularBasis: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  thirdPartyDataStore: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  totalB2BSales: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  totalB2CSales: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  bankName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  bankContactName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  bankPhoneNumber: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isNanp,
    },
  },
  bankAccountNumber: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  bankRoutingNumber: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
});

Commerce.associate = (models) => {
  Commerce.belongsTo(models.identity);
};

module.exports = Commerce;
