const Sequelize = require('sequelize');
const { sequelize } = require('../../database');
const { isNanp, isCountryCode } = require('../../utils/validator');
const { DataTypes } = Sequelize;

const Business = sequelize.define('business', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
  },
  dba: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  legalBusinessName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  ein: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  billingPhoneNumber: {
    type: DataTypes.BIGINT,
    allowNull: false,
    validate: {
      notEmpty: true,
      isNanp,
    }
  },
  businessEmail: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isEmail: true,
    }
  },
  nameOfMainContact: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  billingStreetName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  billingCity: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2],
    },
  },
  billingState: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2],
    },
  },
  billingZip: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  billingCountry: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [2, 2],
      isCountryCode,
    },
  },
  timezone: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  advertisingMethod: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['brochure', 'catalog', 'direct_mail', 'internet', 'newspapers_journals', 'phone', 'tv_radio']]
    },
  },
  whichHostProvider: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  isBusinessSeasonal: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  monthsClosed: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    allowNull: false,
    validate: {
      shouldIncludesMonths(value) {
        if(!value && this.isBusinessSeasonal) {
          throw new Error('monthsClosed should have values when isBusinessSeasonal=true');
        }
      }
    },
  },
  depositRequired: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  valueOfDepositRequired: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      shouldIncludesMonths(value) {
        if(!value && this.depositRequired) {
          throw new Error('valueOfDepositRequired should be included when depositRequired=true');
        }
      }
    },
  },
  refundPolicy: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['dont_have', 'exchange', 'full_refund', 'store_credit']]
    },
  },
  warrantee: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['no', 'partial_refund', 'refund', 'replacement']]
    },
  },
  useFulfillment: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  describeTransactionToFulfillment: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
});

Business.associate = (models) => {
  Business.belongsTo(models.identity);
  Business.hasOne(models.vendor);
};

module.exports = Business;
