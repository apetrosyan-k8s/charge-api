const Sequelize = require('sequelize');
const { sequelize } = require('../../database');
const { DataTypes } = Sequelize;

const Structure = sequelize.define('structure', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
  },
  howManyEmployees: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true,
      min: 1
    },
  },
  typeOfBusiness: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['cbd', 'dropshipping', 'hotel', 'it_services | web_development | software', 'marketing', 'motivational | self_improvement | life_coach', 'nutra_straight_sale', 'online_merchandise', 'travel', 'adult_industry | dating_sites | lingerie | sex_toys', 'alcohol_card_not_present', 'alcohol_card_present', 'antiques', 'auction', 'auto_parts | services | detail', 'charity | donation', 'collections', 'coin_collection | stamp_collection', 'credit_repair', 'debt_consolidation', 'digital_gift_cards | physical_gift_cards', 'direct_sales | multi_level_marketing', 'e_cigarettes', 'event_planning', 'firearms', 'fireworks | explosives', 'furniture', 'gas', 'hemp_topical | hemp_injectable', 'jewelry | handbags', 'online_pharmacy', 'photography_services', 'restaurant', 'student_loan_document_prep', 'tax-preparation_moto', 'timeshares', 'tobacco', 'vape', 'wine', 'other']],
    },
  },
  typeOfBusinessOwnership: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['government', 'limited_liability_company', 'non_profit', 'partnership', 'private_corporation', 'sole_proprietor']],
    },
  },
  stateOfFormation: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  businessStartDate: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isDate: true,
    },
  },
  typeOfLocation: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['business_district | retail', 'office | industrial', 'residential']],
    },
  },
  squareFootageOffice: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn: [['0-250', '251-500', '501-2000', '2000+']],
    },
  },
  numberOfLocations: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  priorBankruptcy: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
  detailedDescriptionOfProducts: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
  },
});

Structure.associate = (models) => {
  Structure.belongsTo(models.identity);
};

module.exports = Structure;
