require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser')
const api = require('./api/v1');
const { handleError } = require('./utils/error');
const db = require('./database');

const app = express();

db
  .init()
  .then(() => {
    app
      .use(bodyParser.json())
      .use('/', api) // might be /api/v1 or something
      .use((err, req, res, next) => handleError(err, res))
      .listen(3000, () => {
        console.log('Server is listening on port 3000');
      })
  }
);
