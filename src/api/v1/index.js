const express = require('express');
const authMiddleware = require('../../middlewares/auth');
const { ErrorHandler } = require('../../utils/error');
const { createIdentity, getIdentities } = require('../../services/identity');
const Queue = require('../../queue');

const api = express.Router();

const NoRequiredFieldsError = () => new ErrorHandler(400, 'Fill required fields!');
const ValidationError = () => new ErrorHandler(400, 'ValidationError');

api.get('/organizations', authMiddleware, async (req, res) => {
  const page = +req.query.page || 1;
  const requestedCount = +req.query.count || 10;
  const outputFormat = req.query.data || 'full';
  const { count, rows } = await getIdentities(page, requestedCount);

  res.status(200).json({
    data: rows.map(identity => getEntityOutput(identity, req.user, outputFormat)),
    meta: {
      currentPage: page,
      count: rows.length,
      rows: count,
      pageCount: Math.ceil(count / requestedCount),
    },
  });
});

api.post('/organizations', authMiddleware, async (req, res, next) => {
  const shouldProcess = req.query.process !== 'false';
  const outputFormat = req.query.data || 'full';
  const identityData = req.body;
  const requiredFields = ['identifier', 'business', 'structure', 'commerce'];
  const status = shouldProcess ? 'processing' : 'review';

  if(requiredFields.some(field => !identityData[field])) {
    throw NoRequiredFieldsError();
  }

  const identity = await createIdentity({ ...identityData, score: 0, status });

  if(!identity) {
    // We can get better errors output if we will provide correct validation messages in models and catch them there
    return next(ValidationError())
  }
  await identity.save();

  if(shouldProcess) {
    Queue.add('process', identity);
  }

  res.status(201).json(getEntityOutput(identity, req.user, outputFormat))
});

function getEntityOutput(identity, user, outputFormat) {
  return {
    id: identity.id,
    identifier: identity.identifier,
    // I believe we should write this in database
    source: {
      method: 'api',
      version: null,
      user: user,
    },
    data: getFormattedData(identity, outputFormat),
    result: {
      status: identity.status,
      score: identity.score,
      // This should be written in database too (additional table with status changes)
      statusHistory: [
        {
          status: identity.status,
          createdAt: identity.createdAt,
          user: user
        },
        {
          status: 'created',
          createdAt: identity.createdAt,
          user: user
        }
      ],
      checks: [],
    },
    notes: [],
    createdAt: identity.createdAt,
    updatedAt: identity.updatedAt,
  };
}

function getFormattedData(data, format) {
  if (format === 'base64') {
    return {
      encodedData: Buffer.from(JSON.stringify(data)).toString('base64'),
    };
  } else if(format === 'full') {
    return data;
  }
  return {};
}

module.exports = api;
