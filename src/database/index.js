const Sequelize = require('sequelize');
const config = require('../utils/config')();

const sequelize = new Sequelize(config.POSTGRES_DB, config.POSTGRES_USER, config.POSTGRES_PASSWORD, {
  host: config.POSTGRES_HOST,
  dialect: 'postgres',
});

function init() {
  return sequelize.sync();
}

module.exports = {
  init,
  sequelize,
}
