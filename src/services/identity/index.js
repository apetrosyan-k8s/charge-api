const { Business, Identity, Vendor, Structure, Commerce, Person } = require('../../models');
const validate = require('../../utils/sequelizeValidator');

const identityInclude = [
  {
    model: Business,
    as: 'business',
    include: [
      {
        model: Vendor,
        as: 'vendor',
      },
    ]
  },
  {
    model: Structure,
    as: 'structure',
  },
  {
    model: Commerce,
    as: 'commerce',
  },
  {
    model: Person,
    as: 'persons',
  }
];

function getIdentities(page, count) {
  if(!page || page < 1) {
    page = 1;
  }
  if(!count || count < 1) {
    count = 10;
  }
  return Identity.findAndCountAll({
    limit: count,
    offset: (page - 1) * count,
    include: identityInclude,
  })
}

function createIdentity(data) {
  const identity = Identity.build(data, {
    include: identityInclude,
  });

  return validate(identity)
    .then(() => identity)
    .catch(() => null);
}

module.exports = {
  getIdentities,
  createIdentity,
}
