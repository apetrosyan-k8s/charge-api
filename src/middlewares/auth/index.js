const { ErrorHandler } = require('../../utils/error');

module.exports = (req, res, next) => {
  // There should be real auth validator
  const authorizationHeader = req.headers.authorization || '';
  const token = authorizationHeader.split(' ')[1];
  if (token !== 'MY_API_KEY') {
    throw new ErrorHandler(401, 'Invalid token!');
  } else {
    // Normally we should decode user token and get the user data
    req.user = {
      "id": "775671ff-f70d-415d-b523-f50b05139ac1",
      "identifier": "albert@quantum.com"
    };
    next();
  }
};
